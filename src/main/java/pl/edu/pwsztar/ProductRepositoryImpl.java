package pl.edu.pwsztar;

import java.util.Collection;
import java.util.HashMap;
import java.util.Optional;

public class ProductRepositoryImpl implements ProductRepository {

    private final HashMap<String, Product> products = new HashMap<>();

    public Collection<Product> getAllProducts() {
        return products.values();
    }

    public Optional<Product> getProductByName(String name) {
        return Optional.ofNullable(products.get(name));
    }

    public void upsert(Product product) {
        products.put(product.getName(), product);
    }

    public boolean deleteByName(String name) {
        return products.remove(name) != null;
    }
}
