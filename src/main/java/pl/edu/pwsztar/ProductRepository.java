package pl.edu.pwsztar;

import java.util.Collection;
import java.util.Optional;

public interface ProductRepository {

    Collection<Product> getAllProducts();

    Optional<Product> getProductByName(String name);

    void upsert(Product product);

    boolean deleteByName(String name);
}
